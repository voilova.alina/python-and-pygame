import urllib.request
import os
import re
import sys

global_path= "INSERT PATH"

def request(url):
    user_agent= "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.101 Safari/537.36"
    headers = {'User-Agent': user_agent}
    request = urllib.request.Request(url,headers = headers)#instead of writing data = None we can specify the name of the next parameter
    response = urllib.request.urlopen(request);
    datatowrite = response.read()
    return datatowrite


def download_chapter():
    url = 'https://mangabox.org/wp-content/uploads/WP-manga/data/manga_5fc12724376c4/88c6f2d9eca2133c7873a497e63c640e/my_hero_academia_1_%d.jpg'
    path = "./" #change folder if necessary
    if not os.path.exists(path):
        os.makedirs(path)
        print("Directory created")
    else:
        print("Directory already exists")
    i = 1
    try:
        while True:
            if not os.path.exists(path+'/bnha%d.jpg' %i):
                print("Downloading images number %d" %i)
                data= request(url %i)

                with open(path+'/bnha%d.jpg' % i, 'wb') as f:
                    f.write(data)
            i+= 1
    except urllib.error.HTTPError as e:
        print(e.code)

def start_download(url):
    htmldata=request(url)
    html_string= htmldata.decode('UTF-8')
    # print(htmldata)
    #() has a special meaning for regular expressions: by () we can adress it as an array
    r = r'<a href="(https://mangabox.org/manga/boku-no-hero-academia-manhua/[a-zA-Z0-9-]+/)" class="btn next_page">\nNext </a>'
    r2 = r'Next'
    next_page_link= None
    searchObj= re.search(r,html_string)
    if searchObj:
        next_page_link= searchObj.group(1)
        print(next_page_link)
    else:
        raise Exception("Error occured")

    r_pic= r'https://mangabox.org/wp-content/uploads/WP-manga/data/manga_5fc12724376c4/[a-z0-9]+/my_hero_academia_[0-9]+_[0-9]+.jpg'
    r_chaper_number=r'https://mangabox.org/wp-content/uploads/WP-manga/data/manga_5fc12724376c4/[a-z0-9]+/my_hero_academia_([0-9]+)_[0-9]+.jpg'
    matches= re.findall(r_pic,html_string)
    #print(len(matches))

    chapter_number =int( re.search(r_chaper_number,html_string).group(1))
    #Creating a chapter folder
    path = global_path+"ch%d" %(chapter_number)
    if not os.path.exists(path):
        os.makedirs(path)
        print("Directory created")
    else:
        print("Directory already exists")

    #Function for download current chapter
    download_chapterS(matches,path)


    print("Downloaded the chapter number", chapter_number, "Do you want to download the next chapter?")
    answer = input()
    if answer == "yes":
        start_download(next_page_link)
    else:
        print("Enjoy your reading")


def download_chapterS(matches,path):
    i = 1
    try:
        for url in matches:
            if not os.path.exists(path+'/bnha%d.jpg' %i):
                print("Downloading images number %d from %d" %(i, len(matches)), end='\r')
                #print("Your url is",url)
                data= request(url)

                with open(path+'/bnha%d.jpg' % i, 'wb') as f:
                    f.write(data)
            i+= 1
    except urllib.error.HTTPError as e:
        print(e.code)


#main function
if len(sys.argv) == 2:
    start_download(sys.argv[1])
else:
    print("You stupid extra!Your forgot the link!")
