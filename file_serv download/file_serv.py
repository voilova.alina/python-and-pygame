import urllib.request
import os
import re
import sys
import requests
import urllib.parse
import xmltodict
import pprint

#class of extention of exception
#need to catch 303 response if we are already log in
class RedirectException(Exception):
    pass

Cookie = "__Host-nc_sameSiteCookielax=true; __Host-nc_sameSiteCookiestrict=true; ociwftjggf7h=bpc6e9c0ief6hnqc1hvocpiu02; oc_sessionPassphrase=BhnsPxCP%2FV%2ByQ%2BJiauV4TBdS3WEBlsHuHVkOq0owtR5X9OMkjm9zKISFdGkK1Y7e%2FJ1dAF2NXWALhExyojsVWQeWk6piSBwM8WB%2FSrWMO%2FVIjWT7v6iAGChVAS9JGEzV"
user_agent = "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:88.0) Gecko/20100101 Firefox/88.0"

headers = {'User-Agent': user_agent,
            'Cookie': Cookie,
            }
Payload = '<?xml version="1.0"?><d:propfind  xmlns:d="DAV:" xmlns:oc="http://owncloud.org/ns" xmlns:nc="http://nextcloud.org/ns"><d:prop><d:getlastmodified /><d:getetag /> <d:getcontenttype /><d:resourcetype /><oc:fileid /><oc:permissions /><oc:size /><d:getcontentlength /><nc:has-preview /><nc:mount-type /></d:prop></d:propfind>'
#TODO: library that convert html string into a struct(?)
propfind_headers = {'User-Agent': user_agent,
                     'Cookie': Cookie,
                     'authorization': "INSERT TOKEN"}

def request(url):
    response = requests.get(url, allow_redirects = False, headers=headers)
    if response.status_code == 303:
        raise RedirectException()
    if response.status_code !=200:
        raise Exception("Got non 200 code. We got: %d"  %response.status_code)
    datatowrite = response.content
    return datatowrite

def login():
    url_loging = "INSERT URL"
    try:
        htmldata = request(url_loging)
    except RedirectException:
        print ("Already log in!")
        return
    html_string = htmldata.decode('UTF-8')
    requesttoken=None
    r= r'name="requesttoken" *value="([^"]+)" */?>'
    searchObj = re.search(r, html_string)
    if searchObj:
        requesttoken = searchObj.group(1)
        print(requesttoken)
    else:
        print(htmldata)
        raise Exception("Error occured")
    mydata ={'requesttoken': requesttoken,'password': "INSERT PASSWORD"}
    re_log= requests.post(url_loging,data = mydata,headers=headers)
    print(re_log.status_code, re_log.reason)

def download_file(url, name, path):

    if not os.path.exists(path):
        os.makedirs(path)
        print("Directory created")
    else:
        print("Directory already exists")
    try:
        if not os.path.exists(path+"/"+name):
            print("Downloading file " + name + " into the folder "+path)
            data= request(url)

            with open(path+"/"+name, 'wb') as f:
                f.write(data)
    except urllib.error.HTTPError as e:
        print(e.code)

def convert_url(url):
    prefix = "/public.php/webdav/"
    index = url.rfind("/")
    substring1 = url[len(prefix):index]
    substring1 = substring1.replace("/","%2F")
    substring2 = url[index+1:]
    string= "INSERT PATH"+substring1+"&files="+substring2
    substring2 = urllib.parse.unquote(substring2)
   # print(string)
    return string,substring2

def download_folder(url, folder):
    req= requests.Request('PROPFIND', url, headers= propfind_headers, data = Payload)
    prepped = req.prepare()

    s = requests.Session()
    resp = s.send(prepped)
    response = resp.text
    mydict = xmltodict.parse(response)
   # pprint.pprint(my_dict)

    #makes iteration for every item in d:response -> 5 times in this case
    #the first folder is the folder we are urrently in-> need to skip it
    for dresponse in mydict["d:multistatus"]["d:response"][1:]:
       # pprint.pprint (dresponse)
        propstat = 0 #to check whether it's dictionary or list of dictionaries
        if isinstance(dresponse["d:propstat"],dict):
            propstat = dresponse["d:propstat"]
        else:
            propstat = dresponse["d:propstat"][0]



        if 'd:getcontenttype' in propstat["d:prop"]:
            fileurl,filename=convert_url(dresponse["d:href"])
           # print("Downloading " + filename)
            download_file(fileurl,filename,folder)
        elif 'd:collection' in propstat["d:prop"]["d:resourcetype"]:
            folder= folder+'/' + urllib.parse.unquote(dresponse["d:href"].split('/')[-2])
            print ("Downloading folder " + dresponse["d:href"]+ " into the folder "+folder)
            download_folder("INSERT LINK"+dresponse["d:href"], folder)
        else:
            pprint.pprint(dresponse)
            raise Exception("Unknown item type")

#main function
login()
download_folder("INSERT URL", ".\\fileServ")

