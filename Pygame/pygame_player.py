
import pygame
import random
import os

WIDTH = 800
HEIGHT = 650
FPS = 30


# Set colors
WHITE = (255, 255, 255)
BLACK = (0, 0, 0)
RED = (255, 0, 0)
GREEN = (0, 255, 0)
BLUE = (0, 0, 255)

# Create game and window(part 1)
pygame.init()
pygame.mixer.init()
screen = pygame.display.set_mode((WIDTH, HEIGHT))
pygame.display.set_caption("My Game")
clock = pygame.time.Clock()

#settings for assets folder
game_folder = os.path.dirname(__file__)#_file_ is where the folder with program code
img_folder = os.path.join(game_folder, 'img') #os.path.join is the path to the folder
player_img = pygame.image.load(os.path.join(img_folder, 'p1_jump.png')).convert()#load the picture of player
background = pygame.image.load(os.path.join(img_folder, 'space.jpg')).convert()
background = pygame.transform.smoothscale(background, screen.get_size())

class Player(pygame.sprite.Sprite):
    def __init__(self):
        pygame.sprite.Sprite.__init__(self)
        self.image = player_img
        self.image.set_colorkey(BLACK)
        self.rect = self.image.get_rect()
        self.rect.center = (WIDTH / 2, HEIGHT/2 )


    def update(self):
        self.rect.x += 5
        if self.rect.left >WIDTH:
            self.rect.right = 0

# Create game and window(cont)
all_sprites = pygame.sprite.Group()
player = Player()
all_sprites.add(player)


# game loop
running = True
while running:
    clock.tick(FPS))
    for event in pygame.event.get():
        # check for closing window
        if event.type == pygame.QUIT:
            running = False

    all_sprites.update()

    # load background image
    screen.blit(background, (0, 0))
    #screen.fill(BLACK)
    all_sprites.draw(screen)
    # flip the screen
    pygame.display.flip()

pygame.quit()