
import pygame
import random

WIDTH = 800
HEIGHT = 650
FPS = 30
LEFT = -5
RIGHT = +5
DOWN = +5
UP= -5

# Set colors
WHITE = (255, 255, 255)
BLACK = (0, 0, 0)
RED = (255, 0, 0)
GREEN = (0, 255, 0)
BLUE = (0, 0, 255)


class Player(pygame.sprite.Sprite):
    def __init__(self):
        pygame.sprite.Sprite.__init__(self)
        self.image = pygame.Surface((50, 50))
        self.image.fill(GREEN)
        self.rect = self.image.get_rect()
        self.rect.center = (WIDTH / 2, 25 )
        self.x_moving = LEFT
        self.y_moving = DOWN

    def update(self):
        self.rect.x = self.rect.x + self.x_moving
        self.rect.y = self.rect.y + self.y_moving
        if self.rect.left == 0:
            self.x_moving = RIGHT
            self.y_moving = DOWN
        elif self.rect.bottom == HEIGHT:
            ##print ('The bottom is = ', self.rect.bottom)
            self.x_moving = RIGHT
            self.y_moving = UP
        elif self.rect.right == WIDTH:
            self.x_moving = LEFT
            self.y_moving = UP
        elif self.rect.top == 0:
            self.x_moving = LEFT
            self.y_moving = DOWN




# Create a game and a window
pygame.init()
pygame.mixer.init()
screen = pygame.display.set_mode((WIDTH, HEIGHT))
pygame.display.set_caption("My Game")
clock = pygame.time.Clock()
all_sprites = pygame.sprite.Group()
player = Player()
all_sprites.add(player)

# Game loop
running = True
while running:
    # Keep the loop at the right speed
    clock.tick(FPS)
    # Process input (events)
    for event in pygame.event.get():
        # check for closing window
        if event.type == pygame.QUIT:
            running = False

    # Update
    all_sprites.update()

    # rendering
    screen.fill(BLACK)
    all_sprites.draw(screen)
    # flip the screen
    pygame.display.flip()

pygame.quit()